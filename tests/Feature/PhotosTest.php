<?php

namespace Tests\Feature;

use App\Models\User;
use http\Client\Response;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PhotosTest extends TestCase
{
    use WithFaker;

    /**
     * @var Collection|User
     */
    private $user;

    /**
     * @var Collection/Model
     */
    private $photos;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = \App\Models\User::factory()->create();
        $this->photos = \App\Models\Photo::factory()->count(10)->for($this->user)->create();
    }

    /**
     * Success create photo.
     * @group photos1
     * @return void
     */
    public function test_can_get_all_photos()
    {
        $user = $this->user->first();
        $request = $this->getJson(route('photos.index',  ['user' => $user]));
        $request->assertSuccessful();
    }
}
