<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UsersTest extends TestCase
{

    use WithFaker;

    /**
     * @var Collection|User
     */
    private $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = \App\Models\User::factory()->create();
    }

    /**
     * Success see user.
     * @group users1
     * @return void
     */
    public function test_can_get_all_users()
    {
        $request = $this->getJson(route('users.index'));
        $request->assertSuccessful();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data', (array)$payload);
        $this->assertCount($this->user->count(), $payload->data);
    }
}
