<?php

namespace Tests\Feature;

use App\Models\Photo;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentsTest extends TestCase
{
    use WithFaker;

    /**
     * @var Collection|User
     */
    private $user;

    /**
     * @var Collection/Photo[]
     */
    private $photos;

    /**
     * @var Collection/Comment[]
     */
    private $comments;

    /**
     * @var Collection|Photo
     */
    private $photo;

    /**
     * @var Collection/Comment
     */
    private $comment;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = \App\Models\User::factory()->create();
        $this->photos = \App\Models\Photo::factory()->count(10)->for($this->user)->create();
        $this->photo = $this->photos->first();
        $this->comments = \App\Models\Comment::factory()->count(10)->for($this->photo)->for($this->user)->create();
    }

    /**
     * Success for user can see comment.
     * @group comments1
     * @return void
     */
    public function test_can_get_all_comments()
    {
        $photo = $this->photos->random();
        $request = $this->getJson(route('comments.index', ['photo' => $photo]));
        $request->assertSuccessful();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data', (array)$payload);
        $this->assertCount($this->comments->count(), $payload->data);
    }

}
