@extends('layouts.app')

@section('content')

    <div class="col main pt-5 mt-3">
        <h1>@lang('messages.add_photo')</h1>
        <form enctype="multipart/form-data" method="post" action="{{ route('photos.store') }}">
            @csrf
            <div class="form-group">
                <label for="title"><b>@lang('messages.title')</b></label>
                <input type="text" class="form-control" id="title" name="title">
                @if ($errors->has('title'))
                    <div class="alert alert-danger">
                        <ul>
                            @error('title')
                            <li>@lang('messages.error')</li>
                            @enderror
                        </ul>
                    </div>
                @endif

            </div>
            <div class="form-group">
                <label for="photo"><b>@lang('messages.photo')</b></label>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile" name="photo">
                    <label class="custom-file-label" for="customFile">@lang('messages.choose')</label>
                    @if ($errors->has('photo'))
                        <div class="alert alert-danger">
                            <ul>
                                @error('photo')
                                <li>@lang('messages.error')</li>
                                @enderror
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
            <button type="submit" class="btn btn-primary">@lang('messages.submit')</button>
        </form>
    </div>
@endsection
