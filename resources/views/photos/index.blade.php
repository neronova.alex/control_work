
@extends('layouts.app')


@section('content')

    <div class="container">
        <div style="padding-bottom: 30px;">
            <h1>@lang('messages.gallery')</h1>
        </div>
        @foreach($photos as $photo)
            <div class="card d-inline-flex col p-4 m-3" style="width: 15rem; height: 25rem">
                <div class="card-img">
                    <a href="{{route('photos.show', ['photo' => $photo])}}">
                        <img src="{{asset('/storage/' . $photo->photo)}}" class="card-img-top" alt="{{$photo->photo}}">
                    </a>
                </div>
                <div class="card-body d-flex flex-column">
                    <div>
                        <div>
                            <a href="{{route('photos.show', ['photo' => $photo])}}"><b>{{$photo->title}}</b></a>
                        </div>
                        <div>
                            <a href="{{route('users.show', ['user' => $photo->user])}}"> @lang('messages.by'): {{$photo->user->name}}</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row justify-content-md-center p-5">
        <div class="col-md-auto">
            {{ $photos->links('pagination::bootstrap-4') }}
        </div>
    </div>

@endsection



