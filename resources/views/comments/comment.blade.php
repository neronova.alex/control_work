
<div class="">
    <div class="media g-mb-30 media-comment">
        <div class="media-body u-shadow-v18 g-bg-secondary g-pa-30">
            <div class="g-mb-15">
                <h5 class="h5 g-color-gray-dark-v1 mb-0">{{$comment->user->name}}</h5>
            </div>
            <p>
                {{$comment->score}}
            </p>
            <p>
                {{$comment->body}}
            </p>
        </div>
    </div>
</div>
