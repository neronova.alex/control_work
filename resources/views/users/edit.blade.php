@extends('layouts.app')

@section('content')

    <div class="col main pt-5 mt-3">
        <h1>@lang('messages.edit')</h1>
        <form enctype="multipart/form-data" method="post" action="{{ route('users.update', ['user' => $user]) }}">
            @method('put')
            @csrf
            <div class="form-group">
                <label for="name"><b>@lang('messages.name')</b></label>
                <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}">
                @if ($errors->has('name'))
                    <div class="alert alert-danger">
                        <ul>
                            @error('name')
                            <li>@lang('messages.error')</li>
                            @enderror
                        </ul>
                    </div>
                @endif
            </div>
            <button type="submit" class="btn btn-primary">@lang('messages.submit')</button>
        </form>
    </div>

@endsection
