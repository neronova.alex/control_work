@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="card-body">
            <h3>{{$user->name}}</h3>
        </div>

        @can('update', $user)
            @if (Auth::user()->id == $user->id)
                    <a href="{{route('users.edit', ['user' => $user])}}" class="btn btn-primary"><b>@lang('messages.edit')</b></a>
            @endif
        @endcan

        @can('create', \App\Models\Photo::class)
            @if (Auth::user()->id == $user->id)
                    <a href="{{route('photos.create')}}" class="btn btn-primary"><b>@lang('messages.add_photo')</b></a>
            @endif
        @endcan

        <div class="card-body">
            <a href="{{route('photos.index')}}" class="btn btn-primary"><b>@lang('messages.back')</b></a>
        </div>

        <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 justify-content-center">
            @foreach($user->photos as $photo)
                <div class="d-flex col mb-3">
                    <div class="card" style="width: 16rem">
                        <div class="card-img">
                            <img src="{{asset('/storage/' . $photo->photo)}}" class="card-img-top" alt="{{$photo->photo}}">
                        </div>
                        <div class="card-body d-flex flex-column">
                            <h5 class="card-title"><b>{{$photo->title}}</b></h5>
                            <div class="mt-auto">
                                <a href="{{route('photos.show', ['photo' => $photo])}}" class="btn btn-primary"><b>@lang('messages.show')</b></a>
                                <div class="d-inline-block">
                                    @can('delete', $photo)
                                        <form method="post" action="{{route('photos.destroy', ['photo' => $photo])}}">
                                            @method('delete')
                                            @csrf
                                            <button type="submit" class="btn btn-primary"><b>@lang('messages.delete')</b></button>
                                        </form>
                                    @endcan
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection
