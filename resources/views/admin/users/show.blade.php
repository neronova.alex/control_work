@extends('layouts.admin')


@section('content')

    <div class="container">
        <div style="padding-bottom: 30px;">
            <h1>User</h1>
        </div>
        <table class="table" style="padding-top: 30px">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">User</th>
                <th scope="col">Email</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">{{$user->id}}</th>
                <td>
                    {{$user->name}}
                </td>
                <td>{{$user->email}}</td>
                <td>
                    <a href="{{route('admin.users.index', ['user' => $user])}}">
                        Back
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
@endsection
