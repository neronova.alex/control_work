@extends('layouts.admin')

@section('content')

    <div class="col main pt-5 mt-3">
        <h1>Add user</h1>
        <form enctype="multipart/form-data" method="post" action="{{ route('admin.users.store') }}">
            @csrf
            <div class="form-group">
                <label for="name"><b>Name</b></label>
                <input type="text" class="form-control" id="name" name="name">
                @if ($errors->has('name'))
                    <div class="alert alert-danger">
                        <ul>
                            @error('name')
                            <li>The field must be filled</li>
                            @enderror
                        </ul>
                    </div>
                @endif
            </div>
            <div class="form-group">
                <label for="email"><b>Email</b></label>
                <input type="email" class="form-control" id="email" name="email">
                @if ($errors->has('email'))
                    <div class="alert alert-danger">
                        <ul>
                            @error('email')
                            <li>The field must be filled</li>
                            @enderror
                        </ul>
                    </div>
                @endif
            </div>
            <div class="form-group">
                <label for="password"><b>Password</b></label>
                <input type="password" class="form-control" id="password" name="password">
                @if ($errors->has('password'))
                    <div class="alert alert-danger">
                        <ul>
                            @error('password')
                            <li>The field must be filled</li>
                            @enderror
                        </ul>
                    </div>
                @endif
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection

