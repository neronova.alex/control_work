
@extends('layouts.admin')


@section('content')

    <div class="container">
        <div style="padding-bottom: 30px;">
            <h1>Users</h1>
        </div>

        <div>
            <a href="{{route('admin.users.create')}}"><b>Add user</b></a>
        </div>

        <table class="table" style="padding-top: 30px">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">User</th>
                <th scope="col">Email</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>

            @foreach($users as $user)
                <tr>
                    <th scope="row">{{$user->id}}</th>
                    <td>
                        <a href="{{route('admin.users.show', ['user' => $user])}}">
                            {{$user->name}}
                        </a>
                    </td>
                    <td>
                        <a href="{{route('admin.users.show', ['user' => $user])}}">
                            {{$user->email}}
                        </a>
                    </td>
                    <td>
                        <a href="{{route('admin.users.edit', ['user' => $user])}}">
                            Edit
                        </a>
                        <a href="{{route('admin.users.show', ['user' => $user])}}">
                            Show
                        </a>
                        <form method="post" action="{{route('admin.users.destroy', ['user' => $user])}}">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-outline-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>

@endsection



