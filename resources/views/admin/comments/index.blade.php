
@extends('layouts.admin')


@section('content')

    <div class="container">
        <div style="padding-bottom: 30px;">
            <h1>Comments</h1>
        </div>

        <table class="table" style="padding-top: 30px">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">User</th>
                <th scope="col">Photo</th>
                <th scope="col">Comment</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>

            @foreach($comments as $comment)
                <tr>
                    <th scope="row">{{$comment->id}}</th>
                    <td>
                        <a href="{{route('admin.comments.show', ['comment' => $comment])}}">
                            {{$comment->user->name}}
                        </a>
                    </td>
                    <td>
                        <img src="{{asset('/storage/' . $comment->photo->photo)}}" alt="{{asset('/storage/' . $comment->photo->photo)}}" width="50px" height="50px">
                    </td>
                    <td>{{$comment->body}}</td>
                    <td>
                        <a href="{{route('admin.comments.show', ['comment' => $comment])}}">
                            Show
                        </a>
                        <form method="post" action="{{route('admin.comments.destroy', ['comment' => $comment])}}">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-outline-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>

        <div class="row justify-content-md-center p-5">
            <div class="col-md-auto">
                {{ $comments->links('pagination::bootstrap-4') }}
            </div>
        </div>

@endsection



