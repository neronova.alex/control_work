@extends('layouts.admin')


@section('content')

    <div class="container">
        <div style="padding-bottom: 30px;">
            <h1>Comment</h1>
        </div>
        <table class="table" style="padding-top: 30px">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">User</th>
                <th scope="col">Photo</th>
                <th scope="col">Comment</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">{{$comment->id}}</th>
                    <td>
                        {{$comment->user->name}}
                    </td>
                    <td>
                        <img src="{{asset('/storage/' . $comment->photo->photo)}}" alt="{{asset('/storage/' . $comment->photo->photo)}}" width="50px" height="50px">
                    </td>
                    <td>{{$comment->body}}</td>
                    <td>
                        <a href="{{route('admin.comments.index', ['comment' => $comment])}}">
                            Back
                        </a>
                    </td>
                </tr>
            </tbody>
            </table>
@endsection
