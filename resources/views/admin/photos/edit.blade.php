@extends('layouts.admin')

@section('content')

    <div class="col main pt-5 mt-3">
        <h1>Edit photo</h1>
        <form enctype="multipart/form-data" method="post" action="{{ route('admin.photos.store') }}">
            @csrf
            <div class="form-group">
                <label for="title"><b>Title</b></label>
                <input type="text" class="form-control" id="title" name="title" value="{{$photo->title}}">
                @if ($errors->has('title'))
                    <div class="alert alert-danger">
                        <ul>
                            @error('title')
                            <li>The field must be filled</li>
                            @enderror
                        </ul>
                    </div>
                @endif

            </div>
            <div class="form-group">Photo</b></label>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile" name="photo" value="{{$photo->photo}}">
                    <label class="custom-file-label" for="customFile">Choose</label>
                    @if ($errors->has('photo'))
                        <div class="alert alert-danger">
                            <ul>
                                @error('photo')
                                <li>The field must be filled</li>
                                @enderror
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection

