@extends('layouts.admin')

@section('content')

    <div class="container">
        <div class="card" style="width: 26rem">
            <img class="card-img-top" src="{{asset('/storage/' . $photo->photo)}}" alt="{{$photo->photo}}">
            <div class="card-body">
                <h5 class="card-title"><b>{{$photo->title}}</b></h5>
                <a href="{{route('users.show', ['user' => $photo->user])}}"><b>@lang('messages.by'): {{$photo->user->name}}</b></a>
                <h6><b>@lang('messages.average'): {{round($average)}}</b></h6>
            </div>
            <div class="card-body">
                <a href="{{route('photos.index')}}" class="btn btn-primary btn-sm"><b>@lang('messages.back')</b></a>
            </div>
        </div>

        <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            @lang('messages.comments')
                        </button>
                    </h5>
                </div>

                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-8 scrollit">
                                @foreach($photo->comments as $comment)
                                    <div class="">
                                        <div class="media g-mb-30 media-comment">
                                            <div class="media-body u-shadow-v18 g-bg-secondary g-pa-30">
                                                <div class="g-mb-15">
                                                    <h5 class="h5 g-color-gray-dark-v1 mb-0">{{$comment->user->name}}</h5>
                                                </div>
                                                <p>
                                                    {{$comment->score}}
                                                </p>
                                                <p>
                                                    {{$comment->body}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(Auth::check())
                <div class="col-4 fixed">
                    <div class="comment-form">
                        <form id="create-comment">
                            @csrf
                            <input type="hidden" id="photo_id" value="{{$photo->id}}">
                            <div class="form-group">
                                <label for="score">@lang('messages.score')</label>
                                <select class="custom-select" name="score" required>
                                    <option value="">@lang('messages.choose').</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="commentFormControl">@lang('messages.comment')</label>
                                <textarea name="body" class="form-control" id="commentFormControl" rows="3" required></textarea>
                            </div>
                            <button id="create-comment-btn" type="submit" class="btn btn-outline-primary btn-sm btn-block">
                                @lang('messages.add_comment')
                            </button>
                        </form>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
