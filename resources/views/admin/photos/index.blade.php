
@extends('layouts.admin')


@section('content')

    <div class="container">
        <div style="padding-bottom: 30px;">
            <h1>Photo gallery</h1>
        </div>

        <div>
            <a href="{{route('admin.photos.create')}}"><b>Add photo</b></a>
        </div>

        <table class="table" style="padding-top: 30px">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Photo</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>



        @foreach($photos as $photo)
            <tr>
                <th scope="row">{{$photo->id}}</th>
                <td>
                    <a href="{{route('admin.photos.show', ['photo' => $photo])}}">
                        {{$photo->title}}
                    </a>
                </td>
                <td>
                    <img src="{{asset('/storage/' . $photo->photo)}}" alt="{{asset('/storage/' . $photo->photo)}}" width="50px" height="50px">
                </td>
                <td>
                    <a href="{{route('admin.photos.edit', ['photo' => $photo])}}">
                        Edit
                    </a>
                    <a href="{{route('admin.photos.show', ['photo' => $photo])}}">
                        Show
                    </a>
                    <form method="post" action="{{route('admin.photos.destroy', ['photo' => $photo])}}">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-outline-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

    <div class="row justify-content-md-center p-5">
        <div class="col-md-auto">
            {{ $photos->links('pagination::bootstrap-4') }}
        </div>
    </div>

@endsection



