$(document).ready(function (){
    $('#create-comment-btn').click(function (event) {
        event.preventDefault();

        const data = $('#create-comment').serialize();
        const photoId = $('#photo_id').val();

        $.ajax({
            url: `/photos/${photoId}/comments`,
            method: "POST",
            data: data
        })
            .done(function (response) {
                renderData(response.comment);
            }).fail(function(response) {
            console.log(response);
        });
        console.log(data);
    });

    function renderData(comment) {
        let commentsBlock = $('.scrollit');
        $(commentsBlock).append(comment);
        clearForm();
    }

    function clearForm() {
        $("#create-comment").trigger('reset');
    }
});
