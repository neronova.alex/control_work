<?php

namespace App\Providers;

use App\Models\Photo;
use App\Models\User;
use App\Policies\AdminPolicy;
use App\Policies\PhotoPolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Photo:: class => PhotoPolicy:: class,
        User:: class => UserPolicy:: class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('view', [AdminPolicy::class, 'view']);
    }
}
