<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;


    /**
     * @param User $user
     * @return mixed
     */
    public function view(User $user)
    {
        return $user->isAdmin;
    }
}
