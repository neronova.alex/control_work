<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PhotoRequest;
use App\Models\Photo;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PhotosController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('view', Auth::user());
        $photos = Photo::orderBy('id', 'desc')->paginate(12);
        return view('admin.photos.index', compact('photos'));
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('view', Auth::user());
        return view('admin.photos.create');
    }


    /**
     * @param PhotoRequest $request
     * @return RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(PhotoRequest $request): RedirectResponse
    {
        $this->authorize('view', Auth::user());
        $photo = new Photo();
        $photo->title = $request->input('title');
        $photo->user()->associate($request->user());
        $file = $request->file('photo');
        if (!is_null($file)) {
            $path = $file->store('pictures', 'public');
            $photo['photo'] = $path;
        }
        $photo->save();

        return redirect()->route('admin.photos.index');
    }


    /**
     * @param Photo $photo
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Photo $photo)
    {
        $this->authorize('view', Auth::user());
        if($photo->comments->count() > 0) {
            foreach ($photo->comments as $comment) {
                $scores[] = $comment['score'];
            }
            $average = collect($scores)->avg();
        } else {
            $average = 0;
        }
        return view('admin.photos.show', compact('photo', 'average'));
    }


    /**
     * @param Photo $photo
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Photo $photo)
    {
        return view('admin.photos.edit', compact('photo'));
    }


    /**
     * @param PhotoRequest $request
     * @param Photo $photo
     * @return RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(PhotoRequest $request, Photo $photo): RedirectResponse
    {
        $this->authorize('view', Auth::user());
        $data = $request->all();
        if ($request->hasFile('picture')) {
            $file = $request->file('picture');
            $path = $file->store('pictures', 'public');
            $data['picture'] = $path;
        }
        $photo->update($data);
        return redirect()->action([self::class, 'show'], ['photo' => $photo]);
    }


    /**
     * @param Photo $photo
     * @return RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Photo $photo)
    {
        $this->authorize('view', Auth::user());
        $photo->delete();
        return redirect()->route('admin.photos.index', $photo->user_id);
    }
}
