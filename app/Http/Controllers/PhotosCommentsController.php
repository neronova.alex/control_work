<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use App\Models\Photo;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PhotosCommentsController extends Controller
{

    /**
     * @param CommentRequest $request
     * @param Photo $photo
     * @return JsonResponse
     */
    public function store(CommentRequest $request, Photo $photo): JsonResponse
    {
        $comment = new Comment();
        $comment->user()->associate($request->user());
        $comment->score = $request->input('score');
        $comment->body = $request->input('body');
        $comment->photo_id = $photo->id;
        $comment->save();
        return response()->json([
            'comment' => view('comments.comment', compact('comment'))->render()], 201);
    }
}
