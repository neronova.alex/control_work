<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhotoRequest;
use App\Models\Photo;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PhotosController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $photos = Photo::orderBy('id', 'desc')->paginate(12);
        return view('photos.index', compact('photos'));
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Photo::class);
        return view('photos.create');
    }


    /**
     * @param PhotoRequest $request
     * @return RedirectResponse
     */
    public function store(PhotoRequest $request): RedirectResponse
    {
        $photo = new Photo();
        $photo->title = $request->input('title');
        $photo->user()->associate($request->user());
        $file = $request->file('photo');
        if (!is_null($file)) {
            $path = $file->store('pictures', 'public');
            $photo['photo'] = $path;
        }
        $photo->save();

        return redirect()->route('photos.index');
    }


    /**
     * @param Photo $photo
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Photo $photo)
    {
        if($photo->comments->count() > 0) {
            foreach ($photo->comments as $comment) {
                $scores[] = $comment['score'];
            }
            $average = collect($scores)->avg();
        } else {
            $average = 0;
        }
        return view('photos.show', compact('photo', 'average'));
    }

    /**
     * @param Photo $photo
     * @return RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Photo $photo)
    {
        $this->authorize('delete', $photo);
        $photo->delete();
        return redirect()->route('users.show', $photo->user_id);
    }
}
