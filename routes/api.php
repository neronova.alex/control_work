<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('photos', \App\Http\Controllers\Api\PhotosController::class);

Route::apiResource('comments', \App\Http\Controllers\Api\CommentsController::class);

Route::apiResource('users', \App\Http\Controllers\Api\UsersController::class);
