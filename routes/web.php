<?php

use App\Http\Controllers\Admin\AdminPagesController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\PhotosController as AdminPhotosController;
use App\Http\Controllers\Admin\CommentsController as AdminCommentsController;
use App\Http\Controllers\Admin\UsersController as AdminUsersController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('language')->group(function () {
    Route::get('/', [App\Http\Controllers\PhotosController::class, 'index'])->name('home');

    Route::resource('photos', \App\Http\Controllers\PhotosController::class)->except(['edit', 'update']);

    Route::resource('users', \App\Http\Controllers\UsersController::class)->only(['show', 'edit', 'update']);

    Route::resource('photos.comments', App\Http\Controllers\PhotosCommentsController::class)
        ->only(['store'])->middleware('auth');

    Auth::routes();
});

Route::prefix('admin')->middleware('auth')->name('admin.')->group(function () {
    Route::resources(
        [
            'photos' => AdminPhotosController::class,
            'comments' => AdminCommentsController::class,
            'users' => AdminUsersController::class
        ]
    );
    Route::get('dashboard', [AdminPagesController::class, 'index'])->middleware('auth')->name('dashboard');
});

Route::get('language/{locale}', [App\Http\Controllers\LanguageSwitcherController::class, 'switcher'])
    ->name('language.switcher')->where('locale', 'en|ru');

