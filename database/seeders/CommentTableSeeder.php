<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\Models\User::all();
        $photos = \App\Models\Photo::all();
        for($i = 0; $i < 10; $i++) {
            \App\Models\Comment::factory()
                ->for($users->random())
                ->for($photos->random())
                ->create();
        }
    }
}
