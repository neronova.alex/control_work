<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PhotoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::all()->each(function ($user) {
            $photos_count = rand(1, 10);
            $comment_count = rand(1, 6);
            \App\Models\Photo::factory()
                ->for(
                    $user
                )->has(
                    \App\Models\Comment::factory()
                        ->for(
                            \App\Models\User::where('id', '!=', $user->id)->first()
                        )->count($comment_count)
                )
                ->count($photos_count)->create();
        });
    }
}
